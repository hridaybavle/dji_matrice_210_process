//Drone

#include "djiMatriceDroneInps.h"

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active),
    sync_atti (sync_policy_atti(1)),
    sync_vel  (sync_policy_vel(1))
{
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    counter = 0;
    activate_aerostack_control = false;

    ros::param::param<bool>("~use_attitude_mode", use_attitude_mode, true);
    std::cout << "using attitude mode " << use_attitude_mode << std::endl;
    ros::param::param<bool>("~use_mpc_mode", use_mpc_mode, false);
    std::cout << "using mpc mode " << use_mpc_mode << std::endl;
    ros::param::param<bool>("~use_mpc_zvelocity_mode", use_mpc_zvelocity_mode, false);
    std::cout << "using mpc with velocity in Z mode " << use_mpc_zvelocity_mode << std::endl;
    ros::param::param<bool>("~use_velocity_mode", use_velocity_mode, false);
    std::cout << "using velocity mode " << use_velocity_mode << std::endl;


    return;
}

void DroneCommandROSModule::close()
{
    return;
}

#ifdef MPC_MODEL_INDENTIFICATION
void DroneCommandROSModule::rcCallback(sensor_msgs::JoyConstPtr msg){
	const float RP_SCALER = 0.32;

	mav_msgs::RollPitchYawrateThrust mav_msg;
	mav_msg.header.stamp = ros::Time::now();
	mav_msg.roll = msg->axes[0] * RP_SCALER;
        mav_msg.pitch = msg->axes[1] * RP_SCALER;
        mav_msg.yaw_rate = 0.0;
        mav_msg.thrust.z = 3.0 * msg->axes[3];

	mav_rpy_thrust_pub_.publish(mav_msg);
}
#endif

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Nod
    this->init();

    DroneModule::open(nIn,moduleName);

    //Publishers
    drone_command_publisher         = n.advertise<sensor_msgs::Joy>("dji_sdk/flight_control_setpoint_generic",1);

    //Subscribers
    CommandSubs                     = n.subscribe("command/high_level", 1, &DroneCommandROSModule::commandCallback, this);
    battery_subscriber              = n.subscribe("battery",1, &DroneCommandROSModule::batteryCallback, this);
    activate_aerostack_control_sub  = n.subscribe("activate_aerostack_control", 1, &DroneCommandROSModule::activateAerostackControl, this);

#ifdef MPC_MODEL_INDENTIFICATION
	rc_sub  = n.subscribe("dji_sdk/rc", 1, &DroneCommandROSModule::rcCallback, this);
	mav_rpy_thrust_pub_  = n.advertise<mav_msgs::RollPitchYawrateThrust>("command/roll_pitch_yawrate_thrust",1);

#endif

    if(use_attitude_mode)
    {
        drone_atti_command_sub.subscribe(n, "command/low_level", 1);
        drone_sync_alti_sub.subscribe(n, "command/dAltitude", 1);
        sync_atti.connectInput(drone_atti_command_sub, drone_sync_alti_sub);
        sync_atti.registerCallback(boost::bind(&DroneCommandROSModule::droneAttiAltiSyncCallback, this, _1, _2));
    }
    else if(use_mpc_mode){
        command_roll_pitch_yawrate_thrust_sub_ = n.subscribe("command/roll_pitch_yawrate_thrust", 1,&DroneCommandROSModule::droneRollPitchYawrateThrustCallback, this);
    }
    else if(use_mpc_zvelocity_mode){
        command_roll_pitch_yawrate_thrust_sub_ = n.subscribe("command/roll_pitch_yawrate_thrust", 1,&DroneCommandROSModule::droneRollPitchYawrateVerticalSpeedCallback, this);
    }

    else if(use_velocity_mode){
        drone_atti_vel_command_sub.subscribe(n, "command/low_level", 1);
        drone_sync_alti_vel_sub.subscribe(n, "command/dAltitude", 1);
        drone_sync_vel_ref_sub.subscribe(n,"droneSpeedRefBroadcast",1);
        sync_vel.connectInput(drone_atti_vel_command_sub, drone_sync_alti_vel_sub, drone_sync_vel_ref_sub);
        sync_vel.registerCallback(boost::bind(&DroneCommandROSModule::droneAttiAltiVelSyncCallback, this, _1, _2, _3));
    }


    //services
    sdk_ctrl_authority_service = n.serviceClient<dji_sdk::SDKControlAuthority> ("dji_sdk/sdk_control_authority");
    drone_task_service         = n.serviceClient<dji_sdk::DroneTaskControl>("dji_sdk/drone_task_control");

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void DroneCommandROSModule::droneAttiAltiSyncCallback(droneMsgsROS::droneAutopilotCommandConstPtr atti_msg,
                                                      droneMsgsROS::droneDAltitudeCmdConstPtr alti_msg)
{

    //Asynchronous module with only one callback
    if(!run())
        return;

    //asking for control permission from dji sdk
    if(counter == 0)
    {
        //if(drone->request_sdk_permission_control())
        dji_sdk::SDKControlAuthority authority;
        authority.request.control_enable=1;
        sdk_ctrl_authority_service.call(authority);

        if(authority.response.result)
        {
            counter++;
            std::cout << "obtained control " << std::endl;
        }
        else
        {
            std::cout << "failed to obtain control" << std::endl;
        }

    }

    double roll_command, pitch_command, yaw_command, thrust_command;

    //sending the angles in radians and ENU frame
    roll_command    = (+1)*atti_msg->roll*M_PI/180;
    pitch_command   = (-1)*atti_msg->pitch*M_PI/180;
    yaw_command     = (-1)*atti_msg->dyaw*M_PI/180;
    thrust_command  = (+1)*alti_msg->dAltitudeCmd;

    //this control byte is based on dji sdk documentation
    //for more details see: https://developer.dji.com/onboard-sdk/documentation/appendix/index.html#control-mode-byte
    sensor_msgs::Joy controlVelYawRate;
    uint8_t flag = (0x00   |                          //DJISDK::VERTICAL_VELOCITY
                    0x00   |                          //DJISDK::HORIZONTAL_ANGLE
                    0x08   |                          //DJISDK::YAW_RATE
                    0x02   |                          //DJISDK::HORIZONTAL_BODY
                    0x00);                            //DJISDK::STABLE_DISABLE

    controlVelYawRate.axes.push_back(roll_command);
    controlVelYawRate.axes.push_back(pitch_command);
    controlVelYawRate.axes.push_back(thrust_command);
    controlVelYawRate.axes.push_back(yaw_command);
    controlVelYawRate.axes.push_back(flag);

    drone_command_publisher.publish(controlVelYawRate);


    return;

}

void DroneCommandROSModule::droneAttiAltiVelSyncCallback(droneMsgsROS::droneAutopilotCommandConstPtr atti_msg,
                                                         droneMsgsROS::droneDAltitudeCmdConstPtr alti_msg,
                                                         droneMsgsROS::droneSpeedsStampedConstPtr vel_msg)
{

    //Asynchronous module with only one callback
    if(!run())
        return;

    //asking for control permission from dji sdk
    if(counter == 0)
    {
        //if(drone->request_sdk_permission_control())
        dji_sdk::SDKControlAuthority authority;
        authority.request.control_enable=1;
        sdk_ctrl_authority_service.call(authority);

        if(authority.response.result)
        {
            counter++;
            std::cout << "obtained control " << std::endl;
        }
        else
        {
            std::cout << "failed to obtain control" << std::endl;
        }

    }

    double roll_command, pitch_command, yaw_command, thrust_command;

    //sending the yaw in radians and ENU frame
    roll_command    = (+1)*vel_msg->speed.dx;
    pitch_command   = (+1)*vel_msg->speed.dy;
    yaw_command     = (-1)*atti_msg->dyaw*M_PI/180;
    thrust_command  = (+1)*alti_msg->dAltitudeCmd;

    //this control byte is based on dji sdk documentation
    //for more details see: https://developer.dji.com/onboard-sdk/documentation/appendix/index.html#control-mode-byte
    sensor_msgs::Joy controlVelYawRate;
    uint8_t flag = (0x00   |                          //DJISDK::VERTICAL_VELOCITY
                    0x00   |                          //DJISDK::HORIZONTAL_ANGLE
                    0x08   |                          //DJISDK::YAW_RATE
                    0x00   |                          //DJISDK::HORIZONTAL_BODY
                    0x00);                            //DJISDK::STABLE_DISABLE

    controlVelYawRate.axes.push_back(roll_command);
    controlVelYawRate.axes.push_back(pitch_command);
    controlVelYawRate.axes.push_back(thrust_command);
    controlVelYawRate.axes.push_back(yaw_command);
    controlVelYawRate.axes.push_back(flag);

    drone_command_publisher.publish(controlVelYawRate);


}

void DroneCommandROSModule::droneRollPitchYawrateThrustCallback(const mav_msgs::RollPitchYawrateThrustConstPtr& msg)
{

    //Asynchronous module with only one callback
    if(!run())
        return;

    //asking for control permission from dji sdk
    if(counter == 0)
    {
        //if(drone->request_sdk_permission_control())
        dji_sdk::SDKControlAuthority authority;
        authority.request.control_enable=1;
        sdk_ctrl_authority_service.call(authority);

        if(authority.response.result)
        {
            counter++;
            std::cout << "obtained control " << std::endl;
        }
        else
        {
            std::cout << "failed to obtain control" << std::endl;
        }

    }

    double roll_command, pitch_command, yaw_command, thrust_command;


    //parameters got from https://github.com/ethz-asl/mav_dji_ros_interface/blob/master/dji_interface/cfg/params.yaml
    double minimum_thrust_ = 10;
    double maximum_thrust_ = 100;
    double thrust_offset_ = 8.945;
    double thrust_coefficient_ = 0.7389;

    //sending the yaw in radians and ENU frame
    roll_command    = (+1)*msg->roll;
    pitch_command   = (-1)*-msg->pitch;
    yaw_command     = (-1)*-msg->yaw_rate;
    thrust_command  = (+1)* thrust_offset_ + msg->thrust.z*thrust_coefficient_;

    if(thrust_command < minimum_thrust_){
      //ROS_WARN_STREAM_THROTTLE(0.1, kScreenPrefix + "Throttle command is below minimum.. set to minimum");
      thrust_command = minimum_thrust_;
    }
    if(thrust_command > maximum_thrust_){
      //ROS_WARN_STREAM_THROTTLE(0.1, kScreenPrefix + "Throttle command is too high.. set to max");
      thrust_command = maximum_thrust_;
  }

    //this control byte is based on dji sdk documentation
    //for more details see: https://developer.dji.com/onboard-sdk/documentation/appendix/index.html#control-mode-byte
    sensor_msgs::Joy controlVelYawRate;
    uint8_t flag = (0x20   |                          //DJISDK::VERTICAL_VELOCITY brefore:
                    0x00   |                          //DJISDK::HORIZONTAL_ANGLE
                    0x08   |                          //DJISDK::YAW_RATE
                    0x02   |                          //DJISDK::HORIZONTAL_BODY
                    0x00);                            //DJISDK::STABLE_DISABLE

    controlVelYawRate.axes.push_back(roll_command);
    controlVelYawRate.axes.push_back(pitch_command);
    controlVelYawRate.axes.push_back(thrust_command);
    controlVelYawRate.axes.push_back(yaw_command);
    controlVelYawRate.axes.push_back(flag);

    drone_command_publisher.publish(controlVelYawRate);


}

void DroneCommandROSModule::droneRollPitchYawrateVerticalSpeedCallback(const mav_msgs::RollPitchYawrateThrustConstPtr& msg)
{

    //Asynchronous module with only one callback
    if(!run())
        return;

    //asking for control permission from dji sdk
    if(counter == 0)
    {
        //if(drone->request_sdk_permission_control())
        dji_sdk::SDKControlAuthority authority;
        authority.request.control_enable=1;
        sdk_ctrl_authority_service.call(authority);

        if(authority.response.result)
        {
            counter++;
            std::cout << "obtained control " << std::endl;
        }
        else
        {
            std::cout << "failed to obtain control" << std::endl;
        }

    }

    double roll_command, pitch_command, yaw_command, thrust_command;


    //parameters got from https://github.com/ethz-asl/mav_dji_ros_interface/blob/master/dji_interface/cfg/params.yaml
    double minimum_thrust_ = -2.0;
    double maximum_thrust_ = 2.0;

    //sending the yaw in radians and ENU frame
    roll_command    = (+1)*msg->roll;
    pitch_command   = (-1)*-msg->pitch;
    yaw_command     = (-1)*-msg->yaw_rate;
    thrust_command  = (+1)*msg->thrust.z;

    if(thrust_command < minimum_thrust_){
      //ROS_WARN_STREAM_THROTTLE(0.1, kScreenPrefix + "Throttle command is below minimum.. set to minimum");
      //thrust_command = minimum_thrust_;
    }
    if(thrust_command > maximum_thrust_){
      //ROS_WARN_STREAM_THROTTLE(0.1, kScreenPrefix + "Throttle command is too high.. set to max");
      //thrust_command = maximum_thrust_;
  }
    //this control byte is based on dji sdk documentation
    //for more details see: https://developer.dji.com/onboard-sdk/documentation/appendix/index.html#control-mode-byte
    sensor_msgs::Joy controlVelYawRate;
    uint8_t flag = (0x00   |                          //DJISDK::VERTICAL_VELOCITY brefore:
                    0x00   |                          //DJISDK::HORIZONTAL_ANGLE
                    0x08   |                          //DJISDK::YAW_RATE
                    0x02   |                          //DJISDK::HORIZONTAL_BODY
                    0x00);                            //DJISDK::STABLE_DISABLE

    controlVelYawRate.axes.push_back(roll_command);
    controlVelYawRate.axes.push_back(pitch_command);
    controlVelYawRate.axes.push_back(thrust_command);
    controlVelYawRate.axes.push_back(yaw_command);
    controlVelYawRate.axes.push_back(flag);

    drone_command_publisher.publish(controlVelYawRate);


}

void DroneCommandROSModule::batteryCallback(const droneMsgsROS::battery &msg)
{
    battery_percent = msg.batteryPercent;
    return;
}

void DroneCommandROSModule::commandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    dji_sdk::SDKControlAuthority authority;
    dji_sdk::DroneTaskControl droneTaskControl;

    /*taking-off*/
    if(msg->command == droneMsgsROS::droneCommand::TAKE_OFF)
    {
        authority.request.control_enable=1;
        sdk_ctrl_authority_service.call(authority);
        if(authority.response.result)
        {
            droneTaskControl.request.task = dji_sdk::DroneTaskControl::Request::TASK_TAKEOFF;
            drone_task_service.call(droneTaskControl);
        }

        if(!droneTaskControl.response.result)
        {
            std::cout << "take off failed" << std::endl;
        }
        else
        {
            std::cout << "taking off " << std::endl;
        }

    }
    /* landing*/
    else if (msg->command == droneMsgsROS::droneCommand::LAND)
    {
        droneTaskControl.request.task = dji_sdk::DroneTaskControl::Request::TASK_LAND;
        drone_task_service.call(droneTaskControl);

        if(!droneTaskControl.response.result)
        {
            std::cout << "landing failed "  << std::endl;
        }
        else
        {
            std::cout << "landing " << std::endl;
            authority.request.control_enable=0;
            sdk_ctrl_authority_service.call(authority);
        }
    }

    return;
}

void DroneCommandROSModule::activateAerostackControl(const std_msgs::Empty &msg)
{
    activate_aerostack_control = true;
}
