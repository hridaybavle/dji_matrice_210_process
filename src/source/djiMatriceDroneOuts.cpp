//#include "rotorsSimulatorDroneOuts.h"
#include "djiMatriceDroneOuts.h"


using namespace std;



////// RotationAngles ////////
RotationAnglesROSModule::RotationAnglesROSModule() : DroneModule(droneModule::active)
{
  init();
  return;
}

RotationAnglesROSModule::~RotationAnglesROSModule()
{

  return;
}

void RotationAnglesROSModule::init()
{
  init_gimbal_yaw_received_ = false;
  init_gimbal_yaw_ = 0;

  ros::param::param<bool>("~use_geodesic_yaw", use_geodesic_yaw_, false);
  ros::param::param<double>("~GEO_LAT_1", GEO_LAT_1, 40.12937800);
  ros::param::param<double>("~GEO_LON_1", GEO_LON_1, -3.879578000);
  ros::param::param<double>("~CART_OFFSET_X_1", CART_OFFSET_X_1, 0.0);
  ros::param::param<double>("~CART_OFFSET_Y_1", CART_OFFSET_Y_1, 0.0);
  ros::param::param<double>("~GEO_LAT_2", GEO_LAT_2, 40.129350552);
  ros::param::param<double>("~GEO_LON_2", GEO_LON_2, -3.879108887);

  std::cout << "use geodesic yaw: " << use_geodesic_yaw_ << std::endl;
  std::cout << "GEO LAT 1: " << GEO_LAT_1 << std::endl;
  std::cout << "GEO LON 1: " << GEO_LON_1 << std::endl;
  std::cout << "CART_OFFSET_X_1: " << CART_OFFSET_X_1 << std::endl;
  std::cout << "CART_OFFSET_Y_1: " << CART_OFFSET_Y_1 << std::endl;
  


  return;
}

void RotationAnglesROSModule::close()
{
  return;
}

void RotationAnglesROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
  //Node
  DroneModule::open(nIn,moduleName);

  //Publisher
  RotationAnglesPubl       = n.advertise<geometry_msgs::Vector3Stamped>("rotation_angles", 1);
  ImuDataPubl        	     = n.advertise<sensor_msgs::Imu>("imu",1);
  RotationAnglesRadiansPub = n.advertise<geometry_msgs::Vector3Stamped>("rotation_angles_radians",1);

  RTKpositionPubl   = n.advertise<nav_msgs::Odometry>("rtk_position_enu",1);
  GPSpositionPubl   = n.advertise<nav_msgs::Odometry>("gps_position_enu",1);
  GeodesicYawPub    = n.advertise<std_msgs::Float32>("geodesic_yaw",1);

  //Subscriber
  RotationAnglesSubs=n.subscribe("dji_sdk/imu", 1, &RotationAnglesROSModule::rotationAnglesCallback, this);
  GimbalAnglesSubs = n.subscribe("dji_sdk/gimbal_angle", 1, &RotationAnglesROSModule::gimbalAnglesCallback, this);
  RTKpositionSubs = n.subscribe("dji_sdk/rtk_position", 1, &RotationAnglesROSModule::RTKpositionCallback, this);
  GPSpositionSubs = n.subscribe("dji_sdk/gps_position", 1, &RotationAnglesROSModule::GPSpositionCallback, this);

  // Clients
  ros::ServiceClient main_cam_client = n.serviceClient<dji_sdk::SetupCameraStream>("dji_sdk/setup_camera_stream");

  usleep(2 * 10e6); // Wait for 5 sec

  dji_sdk::SetupCameraStream srv;
  srv.request.cameraType = 1;
  srv.request.start = 1;

  if (!main_cam_client.call(srv))
  {
    ROS_WARN("Failed to call MAIN CAMERA SERVICE service");
  }

  //Flag of module opened
  droneModuleOpened=true;

  //Auto-Start module
  moduleStarted=true;


  //Updating lastTime
  this->lastTimeRotation = ros::Time::now();

  // Init variables
  gimbal_angles_.vector.x = 0.0;
  gimbal_angles_.vector.y = 0.0;
  gimbal_angles_.vector.z = 0.0;

  dji_first_yaw_measurement_ = false;
  dji_first_yaw_measurement_for_gimbal_ = false;
  first_rtk_measurement_ = false;
  first_gps_measurement_ = false;


  prev_gps_x = 0;
  prev_gps_y = 0;
  prev_gps_z = 0;

  prev_rtk_x = 0;
  prev_rtk_y = 0;
  prev_rtk_z = 0;

  if(use_geodesic_yaw_)
  {
    // DJI RTK/GPS topic information

    // # satellite fix status information
    // NavSatStatus status

    // # Latitude [degrees]. Positive is north of equator; negative is south.
    // float64 latitude

    // # Longitude [degrees]. Positive is east of prime meridian; negative is west.
    // float64 longitude

    // # Altitude [m]. Positive is above the WGS 84 ellipsoid
    // # (quiet NaN if no altitude is available).
    // float64 altitude

    double teta1 = (GEO_LAT_1) * M_PI / 180.0;
    double teta2 = (GEO_LAT_2) * M_PI / 180.0;
    double delta1 = (GEO_LAT_2-GEO_LAT_1) * M_PI / 180.0;
    double delta2 = (GEO_LON_2-GEO_LON_1) * M_PI / 180.0;

    //==================Heading Formula Calculation================//

    double y = sin(delta2) * cos(teta2);
    double x = cos(teta1) * sin(teta2) - sin(teta1) * cos(teta2) * cos(delta2);
    double brng = atan2(y,x);
    //    brng = ( ((int)brng + 360) % 360 );

    ROS_WARN_STREAM_THROTTLE(1, "dji_first_yaw_measurement_");

    dji_first_yaw_ = - (brng - M_PI / 2.0); //TODO: Include magenetic-geographic angle difference

    std::cout << "[INFO] First yaw (bearing): " << dji_first_yaw_ << std::endl;

    if(std::isnan(dji_first_yaw_)){
      ROS_ERROR_STREAM_THROTTLE(1, "NAN in dji_first_yaw_");

      return;
    }

    dji_first_yaw_measurement_ = true;

    //publishing the geodesic first yaw for the msf
    std_msgs::Float32 first_yaw_geodsic;
    first_yaw_geodsic.data = dji_first_yaw_;
    GeodesicYawPub.publish(first_yaw_geodsic);
  }
  //#endif



  //End
  return;
}

//Reset
bool RotationAnglesROSModule::resetValues()
{
  return true;
}

//Start
bool RotationAnglesROSModule::startVal()
{
  return true;
}

//Stop
bool RotationAnglesROSModule::stopVal()
{
  return true;
}

//Run
bool RotationAnglesROSModule::run()
{
  if(!DroneModule::run())
  {
    return false;
  }

  return true;
}

// Callback to read gimbal angle
void RotationAnglesROSModule::gimbalAnglesCallback(const geometry_msgs::Vector3Stamped &msg){

  if(!init_gimbal_yaw_received_)
  {
    init_gimbal_yaw_ = msg.vector.z;
    init_gimbal_yaw_received_ = true;
  }

  float yaw  = msg.vector.z;
  if(yaw - init_gimbal_yaw_ < -180.0)
    yaw = (yaw - init_gimbal_yaw_) + 360;
  else if(yaw - init_gimbal_yaw_ > 180)
    yaw = (yaw - init_gimbal_yaw_) - 360;
  else
    yaw = yaw - init_gimbal_yaw_;

  // Store values
  //gimbal_angles_.vector.x = msg.vector.x;
  //gimbal_angles_.vector.y = msg.vector.y;
  //gimbal_angles_.vector.z = msg.vector.z;

  gimbal_angles_.vector.x = msg.vector.y;
  gimbal_angles_.vector.y =-msg.vector.x;
  gimbal_angles_.vector.z =-yaw;

}

void RotationAnglesROSModule::rotationAnglesCallback(const sensor_msgs::Imu &msg)
{
  //Asynchronous module with only one callback!
  if(!run())
    return;

  if(!use_geodesic_yaw_)
  {
    if(!dji_first_yaw_measurement_)
    {
      tf::Quaternion q(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w);
      tf::Matrix3x3 m(q);
      ROS_WARN_STREAM_THROTTLE(1, "dji_first_yaw_measurement_");


      //convert quaternion to euler angels
      m.getRPY(dji_first_roll_, dji_first_pitch_, dji_first_yaw_);

      if(std::isnan(dji_first_yaw_)){
        ROS_WARN_STREAM_THROTTLE(1, "NAN in dji_first_yaw_");

        return;
      }

      dji_first_yaw_measurement_ = true;
    }
  }

  if(use_geodesic_yaw_)
  {
    if(!dji_first_yaw_measurement_for_gimbal_)
    {
      tf::Quaternion q(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w);
      tf::Matrix3x3 m(q);
      ROS_WARN_STREAM_THROTTLE(1, "dji_first_yaw_measurement_");


      //convert quaternion to euler angels
      m.getRPY(dji_first_roll_, dji_first_pitch_, dji_first_yaw_for_gimbal_);

      if(std::isnan(dji_first_yaw_for_gimbal_)){
        ROS_WARN_STREAM_THROTTLE(1, "NAN in dji_first_yaw_");

        return;
      }

      std::cout << "dji_first_yaw_for_gimbal_ " << dji_first_yaw_for_gimbal_ << std::endl;

      dji_first_yaw_measurement_for_gimbal_ = true;
    }
  }

  //convert quaternion to euler angels
  double dji_roll_enu, dji_pitch_enu, dji_yaw_enu;
  tf::Quaternion q_enu(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w);
  tf::Matrix3x3 m_enu(q_enu);
  m_enu.getRPY(dji_roll_enu, dji_pitch_enu, dji_yaw_enu);

  //deg,   mavwork reference frame
  geometry_msgs::Vector3Stamped RotationAnglesMsgs;
  geometry_msgs::Vector3Stamped RotationAnglesRadiansMsgs;

  RotationAnglesMsgs.header.stamp         = ros::Time::now();
  RotationAnglesRadiansMsgs.header.stamp  = ros::Time::now();

  //convert quaternion msg to eigen
  tf::quaternionMsgToEigen(msg.orientation, quaterniond);

  //------ The below code converts ENU (Mavros) frame to NED (Aerostack) frame ------- ///

  //Rotating the frame in x-axis by 180 degrees
  Eigen::Quaterniond BASE_LINK_TO_AIRCRAFT = mavros::UAS::quaternion_from_rpy(M_PI, 0.0, 0.0);
  quaterniond = quaterniond*BASE_LINK_TO_AIRCRAFT;

  //Rotating the frame in x-axis by 180 deg and in z-axis by 90 axis (accordance to the new mavros update)
  Eigen::Quaterniond ENU_TO_NED = mavros::UAS::quaternion_from_rpy(M_PI, 0.0, M_PI_2);
  quaterniond = ENU_TO_NED*quaterniond;
  //-----------------------------------------------------------------------------------///

  //converting back quaternion from eigen to msg
  tf::quaternionEigenToMsg(quaterniond, quaternion);
  tf::Quaternion q(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
  tf::Matrix3x3 m(q);

  mavros::UAS::quaternion_to_rpy(quaterniond, roll, pitch, yaw);

  //convert quaternion to euler angels
  m.getEulerYPR(yaw, pitch, roll);

  RotationAnglesRadiansMsgs.vector.x =  roll;
  RotationAnglesRadiansMsgs.vector.y =  pitch;
  RotationAnglesRadiansMsgs.vector.z = -yaw;

  RotationAnglesMsgs.vector.x = (double)(+1)*(roll)*180/M_PI;
  RotationAnglesMsgs.vector.y = (double)(+1)*(pitch)*180/M_PI;
  RotationAnglesMsgs.vector.z = (double)(+1)*(yaw)*180/M_PI;


  //converting the yaw which is in the range from -phi to +phi to 0 to 360 degrees
  if( RotationAnglesMsgs.vector.z < 0 )
    RotationAnglesMsgs.vector.z += 360.0;

  publishRotationAnglesValue(RotationAnglesMsgs);
  publishImuData(msg);
  RotationAnglesRadiansPub.publish(RotationAnglesRadiansMsgs);
double correct_yaw;
    if(use_geodesic_yaw_){
  if(dji_yaw_enu - dji_first_yaw_for_gimbal_ < -M_PI)
    correct_yaw = (dji_yaw_enu - dji_first_yaw_for_gimbal_) + 2*M_PI;
  else if(dji_yaw_enu - dji_first_yaw_for_gimbal_ > M_PI)
    correct_yaw = (dji_yaw_enu - dji_first_yaw_for_gimbal_) - 2*M_PI;
  else
    correct_yaw = dji_yaw_enu - dji_first_yaw_for_gimbal_;
}
else{
if(dji_yaw_enu - dji_first_yaw_ < -M_PI)
    correct_yaw = (dji_yaw_enu - dji_first_yaw_) + 2*M_PI;
  else if(dji_yaw_enu - dji_first_yaw_ > M_PI)
    correct_yaw = (dji_yaw_enu - dji_first_yaw_) - 2*M_PI;
  else
    correct_yaw = dji_yaw_enu - dji_first_yaw_;
}

      //std::cout << "correct yaw " << correct_yaw << std::endl;
	//std::cout << "dji_yaw_enu " << dji_yaw_enu << std::endl;
	//std::cout << "dji_first_yaw_for_gimbal_ " << dji_first_yaw_for_gimbal_ << std::endl;


  /***** Publication of transformations *****/ // Rotations are applied with "fixed frames"

  static tf::TransformBroadcaster broadcaster;
  tf::Transform transform;
  // ** This transformation should broadcasted by the estimator (e.g. Robot Localization) **
  //    transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
  //    q.setRPY(roll, - pitch, - yaw);
  //    transform.setRotation(q);
  //    broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "base_link"));

  //transform.setOrigin( tf::Vector3(0.155, 0.069, - 0.161));
  //q.setRPY(- roll + M_PI, pitch, yaw);
  //transform.setRotation(q);
  //broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "ground_ENU"));

  //transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
  //q.setRPY((gimbal_angles_.vector.x * M_PI / 180.0), (gimbal_angles_.vector.y * M_PI / 180.0), gimbal_angles_.vector.z * M_PI / 180.0);
  //transform.setRotation(q);
  //broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "ground_ENU", "main_camera_ENU"));

  transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
  q.setRPY(-(M_PI / 2), 0.0, -(M_PI / 2));
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "main_camera_ENU", "main_camera"));

  transform.setOrigin( tf::Vector3(0.155, 0.069, -0.161) );
  q.setRPY((gimbal_angles_.vector.x * M_PI / 180.0), ((gimbal_angles_.vector.y - 4.4)* M_PI / 180.0), (gimbal_angles_.vector.z - 7.3) * M_PI / 180.0); // Gimbal when reset with RC points a bit towardsthe center
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(),"base_link_stabilized", "main_camera_ENU"));

  transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
  q.setRPY(-dji_roll_enu, -dji_pitch_enu, -correct_yaw);
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "base_link_stabilized"));

  transform.setOrigin( tf::Vector3(0.2, 0.0, - 0.05) );
  q.setRPY(- M_PI / 2, 0, - M_PI / 2);
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "fpv_camera"));

  transform.setOrigin( tf::Vector3(0.19, 0.055, - 0.051) );
  q.setRPY(- M_PI / 2, 0, - M_PI / 2);
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "stereo_left_camera"));

  transform.setOrigin( tf::Vector3(0.19, - 0.055, - 0.051) );
  q.setRPY(0- M_PI / 2, 0, - M_PI / 2);
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "stereo_right_camera"));

  transform.setOrigin( tf::Vector3(0.19, 0.0, - 0.051) );
  q.setRPY(- M_PI / 2, 0, - M_PI / 2);
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "stereo_depth_camera"));

  // Claw tf
  transform.setOrigin( tf::Vector3(0.0, 0.0, 0.26) );
  q.setRPY(0.0, 0.0, 0.0);
  transform.setRotation(q);
  broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "gripper"));

  /********************************************/

  return;
}

void RotationAnglesROSModule::RTKpositionCallback(const sensor_msgs::NavSatFixConstPtr& msg){

  if (msg->status.status < sensor_msgs::NavSatStatus::STATUS_FIX) {
    ROS_WARN_STREAM_THROTTLE(1, "No GPS fix");
    return;
  }

  if (!g_geodetic_converter.isInitialised()) {
    ROS_WARN_STREAM_THROTTLE(1, "No GPS reference point set, not publishing");

    if(use_geodesic_yaw_)
      g_geodetic_converter.initialiseReference(GEO_LAT_1, GEO_LON_1, msg->altitude);
    else if(!use_geodesic_yaw_)
      g_geodetic_converter.initialiseReference(msg->latitude, msg->longitude, msg->altitude);

    return;
  }
  if (!dji_first_yaw_measurement_) {
    ROS_WARN_STREAM_THROTTLE(1, "No IMU initial reference to rotate to Aerostack frame");
    return;
  }
  if(fabs(msg->latitude) < 0.01 || fabs(msg->longitude) < 0.01){
    ROS_WARN_STREAM_THROTTLE(1, "RTK position is latitude and longitude 0.0");
    return;
  }
  //converting the proper ENU velocity frame to the aerostack world frame based on the first yaw measurement
  Eigen::Vector3f dji_proper_ENU_frame;
  Eigen::Vector3f aerostack_frame;
  Eigen::Matrix3f aerostack_frame_rotation_mat;

  //std::cout << "yaw measurement " << yaw_rad << std::endl;
  //std::cout << "first yaw: " << dji_first_yaw_ << std::endl;


  double x, y, z;
  g_geodetic_converter.geodetic2Enu(msg->latitude, msg->longitude, msg->altitude, &x, &y, &z);

  if(!first_rtk_measurement_){
    ROS_WARN_STREAM_THROTTLE(1, "Setting initial prev_ values for outliers in RTK");

    prev_rtk_x = x;
    prev_rtk_y = y;
    prev_rtk_z = z;

    first_rtk_measurement_ = true;


  }
  // Detect outliers in rtk
  if (fabs(prev_rtk_x - x) > OUTLIER_RTK){
    ROS_WARN_STREAM_THROTTLE(1, "ERROR: OUTLIER IN RTK in X");
    return;
  }

  if (fabs(prev_rtk_y - y) > OUTLIER_RTK){
    ROS_WARN_STREAM_THROTTLE(1, "ERROR: OUTLIER IN RTK in Y");
    return;
  }

  if (fabs(prev_rtk_z - z) > OUTLIER_RTK * 2.0){
    ROS_WARN_STREAM_THROTTLE(1, "ERROR: OUTLIER IN RTK in Z");
    return;
  }

  prev_rtk_x = x;
  prev_rtk_y = y;
  prev_rtk_z = z;

  dji_proper_ENU_frame(0) = x;
  dji_proper_ENU_frame(1) = y;
  dji_proper_ENU_frame(2) = z;

  aerostack_frame_rotation_mat(0,0) = cos(dji_first_yaw_);
  aerostack_frame_rotation_mat(1,0) = -sin(dji_first_yaw_);
  aerostack_frame_rotation_mat(2,0) = 0;

  aerostack_frame_rotation_mat(0,1) = sin(dji_first_yaw_);
  aerostack_frame_rotation_mat(1,1) = cos(dji_first_yaw_);
  aerostack_frame_rotation_mat(2,1) = 0;

  aerostack_frame_rotation_mat(0,2) = 0;
  aerostack_frame_rotation_mat(1,2) = 0;
  aerostack_frame_rotation_mat(2,2) = 1;

  aerostack_frame = aerostack_frame_rotation_mat * dji_proper_ENU_frame;

#ifdef MBZIRC_DIFFERENT_OFFSET
  dji_proper_ENU_frame(0) = aerostack_frame(0);
  dji_proper_ENU_frame(1) = aerostack_frame(1);
  dji_proper_ENU_frame(2) = aerostack_frame(2);

  double ox_1_f3 = 5.91;
  double oy_1_f3 = -5.87;
  double ox_2_f3 = 40.0 - 2.88;
  double oy_2_f3 = -8.05;
  double theta = std::atan2(std::fabs(oy_2_f3 - oy_1_f3), std::fabs(ox_2_f3 - ox_1_f3));

  aerostack_frame_rotation_mat(0,0) = cos(theta);
  aerostack_frame_rotation_mat(1,0) = -sin(theta);
  aerostack_frame_rotation_mat(2,0) = 0;

  aerostack_frame_rotation_mat(0,1) = sin(theta);
  aerostack_frame_rotation_mat(1,1) = cos(theta);
  aerostack_frame_rotation_mat(2,1) = 0;

  aerostack_frame_rotation_mat(0,2) = 0;
  aerostack_frame_rotation_mat(1,2) = 0;
  aerostack_frame_rotation_mat(2,2) = 1;

  aerostack_frame = aerostack_frame_rotation_mat * dji_proper_ENU_frame;

  std::cout << "X in frame 2: " << aerostack_frame(0) << std::endl;
  std::cout << "Y in frame 2: " << aerostack_frame(1) << std::endl;
#endif

  nav_msgs::Odometry nav_msgs;

  nav_msgs.header =msg->header;
  nav_msgs.header.frame_id = "odom";
  nav_msgs.child_frame_id = "base_link";

  if(use_geodesic_yaw_)
  {
    nav_msgs.pose.pose.position.x = aerostack_frame(0) + CART_OFFSET_X_1;
    nav_msgs.pose.pose.position.y = aerostack_frame(1) + CART_OFFSET_Y_1;
    nav_msgs.pose.pose.position.z = aerostack_frame(2);
    //std::cout << "Z RTK " << nav_msgs.pose.pose.position.z << std::endl;
  
  }
  else if(!use_geodesic_yaw_)
  {
    nav_msgs.pose.pose.position.x = aerostack_frame(0);
    nav_msgs.pose.pose.position.y = aerostack_frame(1);
    nav_msgs.pose.pose.position.z = aerostack_frame(2);
  }
  nav_msgs.pose.covariance[0]   = 0.009;
  nav_msgs.pose.covariance[7]   = 0.009;
  nav_msgs.pose.covariance[28]  = 0.009;
  nav_msgs.pose.covariance[35]  = 0.009;

  RTKpositionPubl.publish(nav_msgs);
}

void RotationAnglesROSModule::GPSpositionCallback(const sensor_msgs::NavSatFixConstPtr& msg){

  if (msg->status.status < sensor_msgs::NavSatStatus::STATUS_FIX) {
    ROS_WARN_STREAM_THROTTLE(1, "No GPS fix");
    return;
  }
  if(fabs(msg->latitude) < 0.01 || fabs(msg->longitude) < 0.01){
    ROS_WARN_STREAM_THROTTLE(1, "GPS position is latitude and longitude 0.0");
    return;
  }

  if (!g_geodetic_converter.isInitialised()) {
    ROS_WARN_STREAM_THROTTLE(1, "No GPS reference point set, not publishing");

    if(use_geodesic_yaw_)

      g_geodetic_converter_gps.initialiseReference(GEO_LAT_1, GEO_LON_1, msg->altitude);
    else if(!use_geodesic_yaw_)
      g_geodetic_converter_gps.initialiseReference(msg->latitude, msg->longitude, msg->altitude);
    return;
  }
  if (!dji_first_yaw_measurement_) {
    ROS_WARN_STREAM_THROTTLE(1, "No IMU initial reference to rotate to Aerostack frame");
    return;
  }

  //converting the proper ENU velocity frame to the aerostack world frame based on the first yaw measurement
  Eigen::Vector3f dji_proper_ENU_frame;
  Eigen::Vector3f aerostack_frame;
  Eigen::Matrix3f aerostack_frame_rotation_mat;

  //std::cout << "yaw measurement " << yaw_rad << std::endl;
  //std::cout << "first yaw: " << dji_first_yaw_ << std::endl;


  double x, y, z;
  g_geodetic_converter_gps.geodetic2Enu(msg->latitude, msg->longitude, msg->altitude, &x, &y, &z);
  if(!first_gps_measurement_){
    ROS_WARN_STREAM_THROTTLE(1, "Setting initial prev_ values for outliers in GPS");
    prev_gps_x = x;
    prev_gps_y = y;
    prev_gps_z = z;

    first_gps_measurement_ =true;

  }
  // Detect outliers in rtk
  if (fabs(prev_gps_x - x) > OUTLIER_RTK*10){
    ROS_WARN_STREAM_THROTTLE(1, "ERROR: OUTLIER IN GPS in X");

    return;
  }

  if (fabs(prev_gps_y - y) > OUTLIER_RTK){
    ROS_WARN_STREAM_THROTTLE(1, "ERROR: OUTLIER IN GPS in Y");
    return;
  }

  if (fabs(prev_gps_z - z) > OUTLIER_RTK * 2.0){
    ROS_WARN_STREAM_THROTTLE(1, "ERROR: OUTLIER IN GPS in Z");
    return;
  }

  prev_gps_x = x;
  prev_gps_y = y;
  prev_gps_z = z;

  dji_proper_ENU_frame(0) = x;
  dji_proper_ENU_frame(1) = y;
  dji_proper_ENU_frame(2) = z;

  aerostack_frame_rotation_mat(0,0) = cos(dji_first_yaw_);
  aerostack_frame_rotation_mat(1,0) = -sin(dji_first_yaw_);
  aerostack_frame_rotation_mat(2,0) = 0;

  aerostack_frame_rotation_mat(0,1) = sin(dji_first_yaw_);
  aerostack_frame_rotation_mat(1,1) = cos(dji_first_yaw_);
  aerostack_frame_rotation_mat(2,1) = 0;

  aerostack_frame_rotation_mat(0,2) = 0;
  aerostack_frame_rotation_mat(1,2) = 0;
  aerostack_frame_rotation_mat(2,2) = 1;

  aerostack_frame = aerostack_frame_rotation_mat * dji_proper_ENU_frame;

#ifdef MBZIRC_DIFFERENT_OFFSET
  dji_proper_ENU_frame(0) = aerostack_frame(0);
  dji_proper_ENU_frame(1) = aerostack_frame(1);
  dji_proper_ENU_frame(2) = aerostack_frame(2);

  double ox_1_f3 = 5.91;
  double oy_1_f3 = -5.87;
  double ox_2_f3 = 40.0 - 2.88;
  double oy_2_f3 = -8.05;
  double theta = std::atan2(std::fabs(oy_2_f3 - oy_1_f3), std::fabs(ox_2_f3 - ox_1_f3));

  aerostack_frame_rotation_mat(0,0) = cos(theta);
  aerostack_frame_rotation_mat(1,0) = -sin(theta);
  aerostack_frame_rotation_mat(2,0) = 0;

  aerostack_frame_rotation_mat(0,1) = sin(theta);
  aerostack_frame_rotation_mat(1,1) = cos(theta);
  aerostack_frame_rotation_mat(2,1) = 0;

  aerostack_frame_rotation_mat(0,2) = 0;
  aerostack_frame_rotation_mat(1,2) = 0;
  aerostack_frame_rotation_mat(2,2) = 1;

  aerostack_frame = aerostack_frame_rotation_mat * dji_proper_ENU_frame;

  std::cout << "GPS X in frame 2: " << aerostack_frame(0) << std::endl;
  std::cout << "GPS Y in frame 2: " << aerostack_frame(1) << std::endl;
#endif

  nav_msgs::Odometry nav_msgs;

  nav_msgs.header =msg->header;
  nav_msgs.header.frame_id = "odom";
  nav_msgs.child_frame_id = "base_link";

  if(use_geodesic_yaw_)
  {
    nav_msgs.pose.pose.position.x = aerostack_frame(0) + CART_OFFSET_X_1;
    nav_msgs.pose.pose.position.y = aerostack_frame(1) + CART_OFFSET_Y_1;
    nav_msgs.pose.pose.position.z = aerostack_frame(2);
  }
  else if(!use_geodesic_yaw_)
  {

    nav_msgs.pose.pose.position.x = aerostack_frame(0);
    nav_msgs.pose.pose.position.y = aerostack_frame(1);
    nav_msgs.pose.pose.position.z = aerostack_frame(2);
  }

  nav_msgs.pose.covariance[0]   = 0.009;
  nav_msgs.pose.covariance[7]   = 0.009;
  nav_msgs.pose.covariance[28]  = 0.009;
  nav_msgs.pose.covariance[35]  = 0.009;

  GPSpositionPubl.publish(nav_msgs);
}

bool RotationAnglesROSModule::publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs)
{
  if(droneModuleOpened==false)
    return false;

  RotationAnglesPubl.publish(RotationAnglesMsgs);
  return true;
}

bool RotationAnglesROSModule::publishImuData(const sensor_msgs::Imu &ImuData)
{
  if(droneModuleOpened==false)
    return false;

  sensor_msgs::Imu ImuMsg;

  ImuMsg = ImuData;

  //converting the acceleration and velocity in Mavframe (NED frame)
  ImuMsg.linear_acceleration.y*=-1;
  ImuMsg.linear_acceleration.z*=-1;

  ImuMsg.angular_velocity.y*=-1;
  ImuMsg.angular_velocity.z*=-1;

  LatestImuMsg = ImuMsg;

  ImuDataPubl.publish(ImuMsg);
  return true;

}

////// Battery ////////
BatteryROSModule::BatteryROSModule() : DroneModule(droneModule::active)
{
  init();
  return;
}

BatteryROSModule::~BatteryROSModule()
{

  return;
}

void BatteryROSModule::init()
{
  return;
}

void BatteryROSModule::close()
{
  return;
}

void BatteryROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
  //Node
  DroneModule::open(nIn,moduleName);

  //init();


  //Configuration


  //Publisher
  BatteryPubl = n.advertise<droneMsgsROS::battery>("battery", 1, true);


  //Subscriber
  BatterySubs=n.subscribe("dji_sdk/battery_state", 1, &BatteryROSModule::batteryCallback, this);


  //Flag of module opened
  droneModuleOpened=true;

  //Auto-Start module
  moduleStarted=true;

  //End
  return;
}

//Reset
bool BatteryROSModule::resetValues()
{
  return true;
}

//Start
bool BatteryROSModule::startVal()
{
  return true;
}

//Stop
bool BatteryROSModule::stopVal()
{
  return true;
}

//Run
bool BatteryROSModule::run()
{
  if(!DroneModule::run())
  {
    return false;
  }

  return true;
}


void BatteryROSModule::batteryCallback(const sensor_msgs::BatteryState &msg)
{
  //Asynchronous module with only one callback!
  if(!run())
    return;

  droneMsgsROS::battery BatteryMsgs;

  // Read Battery from navdata
  BatteryMsgs.header   = msg.header;
  // minimum voltage per cell used by the "digital battery checker" is 3.55 V
  // msg.voltage is in V
  //float battery_voltage = (msg.voltage);
  //BatteryMsgs.batteryPercent = ((battery_voltage - 14.8)/(16.8-14.8)) * 100;
  BatteryMsgs.batteryPercent = msg.percentage;

  publishBatteryValue(BatteryMsgs);
  return;
}


bool BatteryROSModule::publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs)
{
  if(droneModuleOpened==false)
    return false;

  BatteryPubl.publish(BatteryMsgs);
  return true;
}

////// Altitude ////////
AltitudeROSModule::AltitudeROSModule() : DroneModule(droneModule::active)
{
  init();
  return;
}

AltitudeROSModule::~AltitudeROSModule()
{

  return;
}

void AltitudeROSModule::init()
{


  filtered_derivative_wcb.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);
  filtered_derivative_wcb.reset();

  ros::param::param<bool>("~use_external_laser_alt", use_external_laser_alt, false);
  ros::param::param<bool>("~use_altitude_filtering", use_altitude_filtering, false);

  std::cout << "use_external_laser_alt " << use_external_laser_alt << std::endl;
  std::cout << "use_altitude_filtering " << use_altitude_filtering << std::endl;


  return;
}

void AltitudeROSModule::close()
{
  return;
}

void AltitudeROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
  //Node
  DroneModule::open(nIn,moduleName);

  init();


  //Publisher
  AltitudePub          = n.advertise<droneMsgsROS::droneAltitude>("altitude", 1, true);


  //Subscriber
  if(!use_altitude_filtering)
  {
    if(use_external_laser_alt)
      AltitudeSub      = n.subscribe("lightware_altitude", 1, &AltitudeROSModule::publishLaserAlitudeCallback, this);
    else
      AltitudeSub      = n.subscribe("dji_sdk/height_above_takeoff", 1, &AltitudeROSModule::publishAlitudeCallback, this);
  }
  else
    AltitudeFilteredSub  = n.subscribe("altitudeFiltered",1,&AltitudeROSModule::publishAltitudeFilteredCallback, this);


  //Flag of module opened
  droneModuleOpened=true;

  //Auto-Start module
  moduleStarted=true;

  //End
  return;
}

//Reset
bool AltitudeROSModule::resetValues()
{
  return true;
}

//Start
bool AltitudeROSModule::startVal()
{
  return true;
}

//Stop
bool AltitudeROSModule::stopVal()
{
  return true;
}

//Run
bool AltitudeROSModule::run()
{
  if(!DroneModule::run())
  {
    return false;
  }

  return true;
}

void AltitudeROSModule::publishAlitudeCallback(const std_msgs::Float32 &msg)
{
  //Asynchronous module with only one callback!
  if(!run())
    return;


  ros::Time current_timestamp = ros::Time::now();

  double zraw_t = (-1.0) * msg.data;

  time_t tv_sec; suseconds_t tv_usec;
  {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
  }

  double z_t, dz_t;
  filtered_derivative_wcb.getOutput( z_t, dz_t);

  const float ALTITUDE_OFFSET = 0.27;

  //Read Altitude from Pose
  //AltitudeMsg.header = ros::Time::now();
  // correct  timestamp
  AltitudeMsg.header.stamp  = current_timestamp;
  //Altitude needs to be put in [m], mavwork reference frame!!
  AltitudeMsg.altitude = z_t + ALTITUDE_OFFSET;  // m
  AltitudeMsg.var_altitude   = 0.0;
  // [m/s], mavwork reference frame
  AltitudeMsg.altitude_speed = dz_t;
  AltitudeMsg.var_altitude_speed = 0.0;


  publishAltitude(AltitudeMsg);

  return;
}

void AltitudeROSModule::publishLaserAlitudeCallback(const sensor_msgs::Range &msg)
{
  //Asynchronous module with only one callback!
  if(!run())
    return;


  ros::Time current_timestamp = ros::Time::now();

  double zraw_t = (-1.0) * msg.range;

  time_t tv_sec; suseconds_t tv_usec;
  {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
  }

  double z_t, dz_t;
  filtered_derivative_wcb.getOutput( z_t, dz_t);


  //Read Altitude from Pose
  //AltitudeMsg.header = ros::Time::now();
  // correct  timestamp
  AltitudeMsg.header.stamp  = current_timestamp;
  //Altitude needs to be put in [m], mavwork reference frame!!
  AltitudeMsg.altitude = z_t;  // m
  AltitudeMsg.var_altitude   = 0.0;
  // [m/s], mavwork reference frame
  AltitudeMsg.altitude_speed = dz_t;
  AltitudeMsg.var_altitude_speed = 0.0;


  publishAltitude(AltitudeMsg);

  return;
}


void AltitudeROSModule::publishAltitudeFilteredCallback(const geometry_msgs::PoseStamped &msg)
{
  //Asynchronous module with only one callback!
  if(!run())
    return;


  ros::Time current_timestamp = ros::Time::now();

  double zraw_t = (-1.0) * msg.pose.position.z;

  time_t tv_sec; suseconds_t tv_usec;
  {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
  }

  double z_t, dz_t;
  filtered_derivative_wcb.getOutput( z_t, dz_t);


  //Read Altitude from Pose
  AltitudeMsg.header = msg.header;
  // correct  timestamp
  AltitudeMsg.header.stamp  = current_timestamp;
  //Altitude needs to be put in [m], mavwork reference frame!!
  AltitudeMsg.altitude = z_t;  // m
  AltitudeMsg.var_altitude   = 0.0;
  // [m/s], mavwork reference frame
  AltitudeMsg.altitude_speed = dz_t;
  AltitudeMsg.var_altitude_speed = 0.0;


  publishAltitude(AltitudeMsg);

  return;
}

bool AltitudeROSModule::publishAltitude(const droneMsgsROS::droneAltitude altitudemsg)
{
  if(droneModuleOpened==false)
    return false;

  AltitudePub.publish(altitudemsg);

  return true;
}

